<!doctype html>
<html lang="{{ app()->getLocale() }}" class="full-height">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'ProjectLab')</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <style>@yield('inline-css')</style>
    @yield('extra-header')
</head>
<body class="full-height">
<div class="container-fluid d-flex flex-column full-height">
    @include('layouts.navbar')
    <div class="row">
        <div class="col px-0">
            @section('header')
            <div class="jumbotron bg-dark rounded-0 text-light mb-3">
                    <h2>header</h2>
            </div>
            @show
        </div>
    </div>
    <div class="row">
        <div class="col col-4">
            <div class="row">
                <div class="col">
                    @section('sidebar')
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title h2">
                                    Sidebar
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="list-group">
                                    <a href="#" class="list-group-item list-group-item-action list-group-item-light">item 1</a>
                                    <a href="#" class="list-group-item list-group-item-action list-group-item-light">item 2</a>
                                    <a href="#" class="list-group-item list-group-item-action list-group-item-light">item 3</a>
                                </div>
                            </div>
                        </div>
                    @show
                </div>
            </div>
        </div>
        <div class="col col-8">
            <div class="row">
                <div class="col">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-auto">
        <div class="col px-0">
            @section('footer')
            <div class="jumbotron bg-dark rounded-0 text-light mt-3 mb-0">
                    <h2>Footer</h2>
            </div>
            @show
        </div>
    </div>
</div>
<script>@yield('inline-js')</script>
@yield('extra-footer')
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
