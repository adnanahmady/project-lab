<!doctype html>
<html lang="{{ app()->getLocale() }}" class="full-height">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', 'ProjectLab')</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <style>@yield('inline-css')</style>
    @yield('extra-header')
</head>
<body class="full-height">
<div class="container-fluid">
    @include('layouts.navbar')
    <div class="row mt-5">
        <div class="col align-content-center">
            @yield('content')
        </div>
    </div>
</div>
<script>@yield('inline-js')</script>
@yield('extra-footer')
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
