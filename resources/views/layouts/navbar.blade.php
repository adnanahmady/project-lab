<div class="row sticky-top">
    <div class="col px-0">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow">
            <div class="navbar-branc">
                @auth
                    <a class="navbar-brand mr-0" href="#">{{ auth()->user()->username }}</a>
                    <span class="seperator text-warning" style="font-size: 17pt;">|</span>
                @endauth
                <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Explore Projects</a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('login') }}">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('register') }}">Register</a>
                        </li>
                    @endguest
                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="#">New Project</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Your Projects</a>
                        </li>
                            <form action="{{ url('logout') }}" method="post">
                                @csrf
                                <li class="nav-item">
                                    <button class="nav-link btn btn-link" type="submit">Logout</button>
                                </li>
                            </form>
                        @endauth
                </ul>

                <form class="form-inline">
                    <input class="form-control mr-sm-2" type="search" placeholder="Explore" aria-label="Explore">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Explore</button>
                </form>
            </div>
        </nav>
    </div>
</div>
