@extends('layouts.app')

@section('title', 'Users')

@section('content')
    <ul class="list-group">
        @foreach($users as $user)
            <li
                class="list-group-item
                list-group-item-action
                list-group-item-success"
            >{{ $user->username }}</li>
        @endforeach
    </ul>
@endsection
