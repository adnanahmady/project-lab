<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use ProjectLab\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'slug' => $faker->slug,
        'title' => $faker->title,
        'description' => $faker->text,
        'access' => $faker->randomElement(['public', 'protected', 'private'])
    ];
});
